from tkinter import *
from tkinter.messagebox import *

app = Tk()
app.title("QQBot日志")
app.iconphoto(False, PhotoImage(file='icon.png'))

def del_log():
    open("logs.log", 'w').close()
    f = open("logs.log", 'w',encoding="utf-8")
    f.write("[QQBot日志]\n")
    showinfo(title='QQBot日志', message='清除完毕！')
    app.quit()

title1 = Label(app,text="功能")
title1.pack()

del_btn = Button(app,text="清除日志",command=del_log)
del_btn.pack()

title2 = Label(app,text="日志")
title2.pack()

list = Listbox(app,width=100)
sl = Scrollbar(app)
sl.pack(side=RIGHT,fill = Y)
list['yscrollcommand'] = sl.set
with open('logs.log', 'r',encoding="utf-8") as f:
    lines = f.readlines()
    for line in lines:
        list.insert(END, line.strip())
    f.close()
list.pack()
sl['command'] = list.yview

app.mainloop()