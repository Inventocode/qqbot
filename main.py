# 引入库
import requests
import json
import time

# 日志系统
# 日志
def log(text):
    log = open("./logs.log","a")
    log.write(t()+"[INFO] "+text+"\n")
    log.close
    print(t()+"[INFO] "+text)
# 警告
def warn(text):
    log = open("./logs.log","a")
    log.write(t()+"[WARNING] "+str(text)+"\n")
    log.close
    print("\033[33m"+t()+"[WARNING] "+str(text)+"\033[0m")
# 错误
def error(text):
    log = open("./logs.log","a")
    log.write(t()+"[ERROR] "+str(text)+"\n")
    log.close
    print("\033[31m"+t()+"[ERROR] "+str(text)+"\033[0m")

# 时间
def t():
    return(time.strftime('[%Y-%m-%d %H:%M:%S] ', time.localtime()))

# 发送私聊消息
def send_private_msg(id,msg,echo=True):
    api = requests.post('http://127.0.0.1:5700/send_private_msg', data={"user_id": id,"message":msg})
    if echo:
        log("给id为"+str(id)+"的好友发送消息，内容："+str(msg))

# 发送群聊消息
def send_group_msg(id,msg,echo=True):
    api = requests.post('http://127.0.0.1:5700/send_group_msg', data={"group_id": id,"message":msg})
    if echo:
        log("给id为"+str(id)+"的群聊发送消息，内容："+str(msg))

# 获取好友列表
def get_friend_list(echo=True):
    api = requests.post('http://127.0.0.1:5700/get_friend_list', data={})
    if echo:
        log("获取好友列表")
    return(json.loads(api.text).get("data"))

# 获取群聊最新几条消息
def get_group_msg_history(id,echo=True):
    api = requests.post('http://127.0.0.1:5700/get_group_msg_history', data={"group_id":id})
    if echo:
        log("获取群聊最新几条消息")
    return(json.loads(api.text).get("data").get("messages"))

# 撤回消息
def delete_message(message_id,echo=True):
    api = requests.post('http://127.0.0.1:5700/delete_msg',data={"message_id":message_id})
    if echo:
        log("撤回id为"+str(message_id)+"的消息")

group = 238677053
# 循环
while True:
    # 侦测关键词
    new_msg = get_group_msg_history(group,False)[-1]
    msg = new_msg.get("message")
    if msg[:3] == "Bot":
        command = msg[4:]
        # 判断指令
        if command == "help":
            send_group_msg(group,"QQBot v0.1 帮助\n指令\nhelp 显示帮助")
        elif command == "egg":
            send_group_msg(group,"恭喜彩蛋发现了你")
            warn("有个傻逼被彩蛋发现了")
        elif command == " egg":
            send_group_msg(group,"恭喜你发现了真正的彩蛋")
        else:
            send_group_msg(group,"指令不存在")
    # 延迟
    time.sleep(0.001)